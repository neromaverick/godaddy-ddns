# Purpose

On a home network and need a static DNS? Tired of using duckdns, synology, dyndns, etc. and finally invested in a proper domain with Godaddy?

Got an environment that uses Docker, Docker Compose?

This should make all of the issues with public dynamic DNS offerings and put the power to update your WAN IP in your own hands.

## Initial Requirements

### Docker

Install Docker & Docker Compose

https://docs.docker.com/compose/install/

https://docs.docker.com/get-docker/

### Running Directly

It's suggested to run this in Docker with Docker Compose... However you could just take the script and run it in a cron if you want.

If you do, the following packages are needed
```
curl
jq
```

You will need to set the required variables as either environmental vars, or in a vars file read by the script.

## Usage

### Vairables

```
API_KEY=<Godaddy API Key>
API_SECRET=<Godaddy API Secret>
API_URL=<Godaddy API URL>
LAST_WAN_CACHE=<location of last used WAN cache file> (ie. /tmp/ddns-wan-cache)
DOMAIN=<domain name>
HOST=<A record used for ddns>
WANLOOKUP=<list of hosts to lookup wan> (Randomly used - space separated)
CHECKINTERVAL=<interval between checks> (default 300 - 5min)
```

Defaults (Set in Dockerfile)
```
API_URL=https://api.godaddy.com/v1/domains/
LAST_WAN_CACHE=/data/last-wan-cache
WANLOOKUP="ifconfig.me api.ipify.org icanhazip.com ipinfo.io/ip ipecho.net/plain"
CHECKINTERVAL=300
```

Required Variables (Set in docker-compose.yml, env, or vars_file)
```
API_KEY=<Godaddy API Key>
API_SECRET=<Godaddy API Secret>
DOMAIN=<domain name>
HOST=<A record used for ddns>
```

### Docker Compose
Make sure you are in the main directory for this project
You should see
```
scripts/
docker-compose.yml.example
```
Copy docker-compose.yml.example to docker-compose.yml & edit the required variables
```
cp docker-compose.yml.example docker-compose.yml
## Edit docker-compose.yml
```

Build the image
```
docker-compose build
```

Start the container
```
docker-compose up
```
Check the logs and make sure everything looks good, if it is Ctrl+C the container and run it in the background
```
docker-compose up -d
```

### Running Directly
Create the vars_file with the above variables set OR set all the above vars as environment variables

Run the script with the following

(vars_file)
```
VARS_FILE=<path to vars file> script/update-ddns.sh
```
(Environmental Vars)
```
script/update-ddns.sh
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
