#!/usr/bin/env bash

if [ "${VARS_FILE}" ]; then
  source ${VARS_FILE}
fi

checkvars () {
  [ -z "${API_KEY}" ]        && echo "[$(logdate)] [checkvars]: API_KEY is not set"
  [ -z "${API_SECRET}" ]     && echo "[$(logdate)] [checkvars]: API_SECRET is not set"
  [ -z "${API_URL}" ]        && echo "[$(logdate)] [checkvars]: API_URL is not set"
  [ -z "${LAST_WAN_CACHE}" ] && echo "[$(logdate)] [checkvars]: LAST_WAN_CACHE is not set"
  [ -z "${DOMAIN}" ]         && echo "[$(logdate)] [checkvars]: DOMAIN is not set"
  [ -z "${HOST}" ]           && echo "[$(logdate)] [checkvars]: HOST is not set"
  [ -z "${WANLOOKUP}" ]      && echo "[$(logdate)] [checkvars]: WANLOOKUP is not set"
  [ -z "${CHECKINTERVAL}" ]  && echo "[$(logdate)] [checkvars]: CHECKINTERVAL is not set"
}

clearvars () {

  WANIP=
  host=
  API_DATA=
  API_WANIP=
  CACHE_WANIP=
  SHUFHOSTS=

}

logdate () {
  date "+%Y/%m/%d %H:%M:%S"
}

wan-lookup () {

  SHUFHOSTS=$(echo "${WANLOOKUP}"|sed 's/ /\n/g'|sort -R)

  i=1
  while [ -z ${WANIP} ]; do

    host=$(echo ${SHUFHOSTS}| awk '{print $'${i}'}')

    if [ -z ${host} ]; then
      echo "[$(logdate)] [wan-lookup]: All lookup hosts failed... exiting"
      exit 1
    fi

    WANIP=$(curl -s ${host} | grep -E '(([0-9]{1,3})\.){3}([0-9]{1,3}){1}'  | grep -vE '25[6-9]|2[6-9][0-9]|[3-9][0-9][0-9]' | grep -Eo '(([0-9]{1,2}|1[0-9]{1,2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]{1,2}|1[0-9]{1,2}|2[0-4][0-9]|25[0-5]){1}')

    if [ -z ${WANIP} ]; then
      echo "[$(logdate)] [wan-lookup]: [${host}] - Lookup failed"
    else
      echo "[$(logdate)] [wan-lookup]: [${host}] - (${WANIP})"
    fi

    i=$(($i+1))
  done
}

wan-lookup-cache () {

  if [ -f "${LAST_WAN_CACHE}" ]; then
    CACHE_WANIP=$(cat "${LAST_WAN_CACHE}")
    echo "[$(logdate)] [wan-lookup-cache]: [${LAST_WAN_CACHE}] - (${CACHE_WANIP})"
  fi

}

wan-lookup-api () {

  API_DATA=$(curl -s -X GET -H "Authorization: sso-key ${API_KEY}:${API_SECRET}" "${API_URL}/${DOMAIN}/records/A/${HOST}")
  API_WANIP=$(echo "${API_DATA}" | jq -r .[0].data )

  echo "[$(logdate)] [wan-lookup-api]: [${HOST}] - (${API_WANIP})"

}

set-wan-cache () {

  echo "[$(logdate)] [set-wan-cache]: Saving WAN IP to cache file (${LAST_WAN_CACHE})"
  echo "${API_WANIP}" > "${LAST_WAN_CACHE}"

}

set-wan-api () {

  echo "[$(logdate)] [set-wan-api]: Setting WAN IP (${WANIP} -> ${API_WANIP})"

  curl -s -X PUT "${API_URL}/${DOMAIN}/records/A/${HOST}" -H "Authorization: sso-key ${API_KEY}:${API_SECRET}" -H "Content-Type: application/json" -d "[{\"data\": \"${WANIP}\"}]"
  wan-lookup-api

  if [ ${WANIP} == ${API_WANIP} ]; then
    echo "[$(logdate)] [set-wan-api]: Successfully set WAN IP (${API_WANIP})"
  else
    echo "[$(logdate)] [set-wan-api]: Failed to set WAN IP (${WANIP} -> ${API_WANIP})"
  fi

}

main () {

  clearvars
  wan-lookup
  wan-lookup-cache

  if [ "${WANIP}" == "${CACHE_WANIP}" ]; then
    echo "[$(logdate)] [main]: WAN IP has not changed from cache (${CACHE_WANIP})"
  else

    wan-lookup-api

    if [ "${WANIP}" == "${API_WANIP}" ]; then
      echo "[$(logdate)] [main]: WAN IP has not changed from ${HOST}.${DOMAIN} (${API_WANIP})"
      set-wan-cache
    else
      echo "[$(logdate)] [main]: WAN IP has changed (${WANIP} -> ${API_WANIP})"
      set-wan-api
      set-wan-cache
    fi
  fi

}

main-loop () {
  while true; do
    main
    sleep ${CHECKINTERVAL}
  done
}

FAILMSG="$(checkvars)"

if [ "${FAILMSG}" ]; then
  echo "${FAILMSG}"
  exit 1
else
  main-loop
fi
